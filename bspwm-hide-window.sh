#! /usr/bin/env bash

if [[ -f /tmp/hidden-window ]]; then
    wid=$(cat /tmp/hidden-window)
    # xdo show "$wid"
    bspc node $wid --flag "hidden"
    rm -f /tmp/hidden-window
else
    wid=$(bspc query -N -n)
    # xdo hide "$wid"
    bspc node $wid --flag "hidden"
    echo "$wid" > /tmp/hidden-window
fi
