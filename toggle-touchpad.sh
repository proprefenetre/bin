#!/bin/sh

# dev=$(xinput | rg -i ".*touchpad.*id=([0-9]+).*" -r '$1')
dev=$(xinput | rg -i ".*DLL.*id=([0-9]+).*" -r '$1')
state=$(xinput list-props "$dev" | grep "Device Enabled" | awk '{print $4}')

if [ "$state" -eq 1 ] ; then
    xinput disable "$dev"
    notify-send -a "Touchpad" " " "Disabled"
else
    xinput enable "$dev"
    notify-send -a "Touchpad" " " "Enabled"
fi

