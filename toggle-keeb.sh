#!/bin/sh

dev=$(xinput | rg -i ".*AT Translated Set 2 Keyboard.*id=([0-9]+).*" -r '$1')
state=$(xinput list-props "$dev" | grep "Device Enabled" | awk '{print $4}')

if [ "$state" -eq 1 ] ; then
    xinput disable "$dev"
    notify-send -a "Laptop keyboard" " " "Disabled"
else
    xinput enable "$dev"
    notify-send -a "Laptop keyboard" " " "Enabled"
fi
