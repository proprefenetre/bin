#! /usr/bin/env bash

# Curses or tiles
VERSION="Tiles"

url=$(curl -s https://api.github.com/repos/CleverRaven/Cataclysm-DDA/releases | jq -r ".[10].assets | map(select(.name | test(\"cdda-linux-tiles-x64.*\"))) | .[].browser_download_url")

echo "Downloading: $url"

curl -L $url | tar xz
