#!/usr/bin/env bash

id=($(xdotool search --classname "scratchterm"))

if [[  ${#id[@]} -gt 1 ]]; then
    delete=${id[1]}
    id=(${id[@]/$delete})
    bspc node $delete --kill
fi

if [ -z "$id" ]; then
    kitty --name "scratchterm"
    id=($(xdotool search --classname "scratchterm"))
fi

cur_monitor=$(bspc query -M -m focused)

bspc node "${id[0]}" --flag "hidden" -m "$cur_monitor" -f
